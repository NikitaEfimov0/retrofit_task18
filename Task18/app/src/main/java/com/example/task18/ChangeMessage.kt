package com.example.task18

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.navigation.findNavController
import com.example.task18.databinding.FragmentChangeMessageBinding
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ChangeMessage : Fragment() {

    lateinit var binding: FragmentChangeMessageBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentChangeMessageBinding.inflate(inflater)

        binding.mesView.setText(CurrentMessage.message)
        binding.titleView.setText(CurrentMessage.title)

        binding.exit.setOnClickListener {
            binding.root.findNavController().navigate(R.id.toHomeFromChange)
        }

        binding.post.setOnClickListener {
            CurrentMessage.title = binding.titleView.text.toString()
            CurrentMessage.message = binding.mesView.text.toString()
            urlEncoded()
            binding.root.findNavController().navigate(R.id.toHomeFromChange)
        }



        return binding.root
    }

    fun urlEncoded() {

        // Create Retrofit
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .build()

        // Create Service
        val service = retrofit.create(APIINterface::class.java)

        // Create HashMap with fields

        CoroutineScope(Dispatchers.IO).launch {
            // Do the POST request and get response
            val response = service.pushPost(
                CurrentMessage.userId,
                CurrentMessage.id,
                CurrentMessage.title,
                CurrentMessage.message
            )
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    Toast.makeText(requireContext(), "Successfully posted", Toast.LENGTH_SHORT)
                        .show()


                }
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = ChangeMessage()
    }
}
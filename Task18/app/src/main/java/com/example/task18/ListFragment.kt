package com.example.task18

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.task18.databinding.FragmentListBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ListFragment : Fragment() {
    val BASE_URL = "https://jsonplaceholder.typicode.com/"
    lateinit var binding: FragmentListBinding
    lateinit var myAdapter: MyAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var recView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentListBinding.inflate(inflater)
        linearLayoutManager = LinearLayoutManager(binding.root.context)
        recView.layoutManager = linearLayoutManager
        getMyData()
        return binding.root
    }


    private fun getMyData() {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(APIINterface::class.java)

        val retrofitData = retrofitBuilder.getData()

        retrofitData.enqueue(object : Callback<List<MyDataItem>?> {
            override fun onResponse(
                call: Call<List<MyDataItem>?>?,
                response: Response<List<MyDataItem>?>?
            ) {
                val responseBody = response?.body()!!

                myAdapter = MyAdapter(responseBody)
                myAdapter.notifyDataSetChanged()
                recView.adapter = myAdapter



            }

            override fun onFailure(call: Call<List<MyDataItem>?>?, t: Throwable?) {

            }
        })
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) = ListFragment()
    }
}
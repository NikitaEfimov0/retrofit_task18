package com.example.task18

import android.content.Context
import android.media.session.PlaybackState
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView

class MyAdapter(val userList:List<MyDataItem>):
    RecyclerView.Adapter<MyAdapter.ViewHolder>() {
    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        var title: TextView
        var id:TextView
        var card:CardView
        init {
            id = itemView.findViewById(R.id.numberOfMessage)
            title = itemView.findViewById(R.id.messageTitle)
            card = itemView.findViewById(R.id.card)

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var itemView = LayoutInflater.from(parent.context).inflate(R.layout.row_items, parent, false)
        return ViewHolder(itemView)
    }



    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = userList[position].title
        holder.id.text = userList[position].id.toString()
        holder.card.setOnClickListener{
            CurrentMessage.message = userList[position].body
            CurrentMessage.title = userList[position].title
            CurrentMessage.id = userList[position].id
            CurrentMessage.userId = userList[position].userId
            holder.itemView.findNavController().navigate(R.id.toChangeFromHome)
        }
    }

    override fun getItemCount(): Int {
        return userList.size
    }

}
package com.example.task18

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface APIINterface {

    @GET("posts")
    fun getData():Call<List<MyDataItem>>

    @FormUrlEncoded
    @POST("posts")
    suspend fun pushPost(
        @Field("userId") userId:Int,
        @Field("id") id:Int,
        @Field("title") title:String,
        @Field("body") body:String
    ):Response<MyDataItem>
}